In the following exercise, you'll set up Orthanc server and use it to practice dcmtk, pynetdicom3 and pydicom.



Pydicom - the Python library for reading/writing dicom files

Pynetdicom3 - the Python library for making dicom requests

DCMTK - an open-source toolkit for making dicom requests/setup up a store service



1. Ask for CT data from the team - a good opportunity to use Azure Blob Storage Explorer to download data.

2. Setup an Orthanc server (details in the next note)

3. Insert dicom data to it

4. Setup a StoreSCP service (details in the last node).

5. Using the StoreSCP that you set up in the last bullet, C-MOVE a dicom file from ORTHANC to the STORESCP

6. Write a small deidentification module using Pydicom (ask the team how to handle the fields)  

note that in steps 3,5 - you need to do it in both ways:

with DCMTK from the terminal
with Pynetdicom3 / pydicom