# DICOM Toolkit tutorial

This tutorial covers the basics of PACS debugging using DCMTK, a collection of libraries and applications implementing large parts of the DICOM standard.     
As the title implies, before going through this howto, it is recommended to get a basic understanding of [DICOM standard](https://www.dicomstandard.org/) and [PACS](https://en.wikipedia.org/wiki/Picture_archiving_and_communication_system) first. 

<br/>

---
## Step 1 - Run Local Orthanc PACS Server
>Orthanc is an open-source PACS server that comes with a web UI for browsing images. It is a useful project for development purposes (using it as a PACS in dev and staging environments, practicing DICOM commands, etc).  

1. Download and install Orthanc from the official Orthanc website: https://www.orthanc-server.com
2. Set up your local Orthanc server:  
   1. Open *configOSX.json* file that should be found in the main Orthanc folder on your computer.  
   2. Find and remember the following configurations:  
      - `Http Port`: The port you will be using to reach Orthanc GUI. The default should be **8042**.
      - `DicomAet`: The DICOM Application Entity Title. The default should be **ORTHANC**.
      - `DicomPort`: The port you will be used by DCMTK to communicate by DICOM standard protocol. The default should be **4242**.
   3. Configure a new modality by adding the following entry to DicomModalities section:  
   `[“name”]: [“AET”, “IP”, PORT]`
   example: `[“yonatan”]: [“YONATAN”, “127.0.0.1”, 2000]`   
      - `name`: The modality name.
      - `AET`: The Application Entity Title (AE title) of your new modality. Think of it as the same as the hostname of your modality.
      - `IP`: The IP address of the new modality. You should use *localhost - 127.0.0.1*.
      - `PORT`: The port the modality will listen on.
3. Start your Orthanc server by running the following command:  `./startOrthanc.command`
---
## Step 2 - Send DICOM files to your Orthanc server using *storescu*:
>The storescu application implements a Service Class User (SCU) for the Storage Service Class. For each DICOM file on the command-line it sends a C-STORE message to a Storage Service Class Provider (SCP) and waits for a response.  
DCMTK documentation: https://support.dcmtk.org/docs/storescu.html  

1. In your terminal, go to a directory that have some DICOM files.
2. Run the command:  
`storescu localhost 4242 +sd ./ -v -xv -aec ORTHANC -aet YONATAN`  
   >You should replace the AET ("*YONATAN*") with the AET you configured in your Orthanc configOSX file.
    - `localhost` = Orthanc server hostname
    - `4242` = Orthanct server port
    - `+sd` = scan directories for input files (“./“ = upload all *.dcm files in the current directory)
    - `-v` = print processing details (verbose mode)
    - `-xv` = propose JPEG 2000 lossless TS and all uncompressed transfer syntaxes
    - `-aec` = the called AE title of peer
    - `-aet` = the calling AE title
---
## Step 3 - Run findscu to query Orhanc server for Dicom files:  
>The findscu application implements an SCU for the Query/Retrieve Service Class using the C-FIND message. It sends query keys to an SCP and awaits responses.  

1. In your terminal, run the command:  
  `findscu localhost 4242 -v -S -aec ORTHANC -aet YONATAN -k QueryRetrieveLevel=STUDY -k StudyDate -k StudyDescription -k StudyInstanceUID`  
   >You should replace the AET ("*YONATAN*") with the AET you configured in your Orthanc configOSX file.
      - `localhost` = Orthanc server hostname
      - `4242` = Orthanct server port
      - `-v` = print processing details (verbose mode)
      - `-S` = use study root information model
      - `-aec` = the called AE title of peer
      - `-aet` = the calling AE title
      - `-k` = matching keys for data filtering and retrieving. If you provide a key name and a value, it will function as a query filter.
         - `QueryRetrieveLevel` = what to search for
         - `StudyDate` / `StudyDescription` / `StudyInstanceUID` = “columns” to print for each result record  
2. *Optional Example 1* - If you want to query patients instead of studies, run this command:   
`findscu localhost 4242 -v -P -aec ORTHANC -aet YONATAN -k QueryRetrieveLevel=Patient -k PatientID -k PatientName`
3. *Optional Example 2* - If you want to query all patients filtered by id prefix "35*", run the command:  
`findscu localhost 4242 -v -P -aec ORTHANC -aet YONATAN -k QueryRetrieveLevel=Patient -k PatientID="35*" -k PatientName`  
---
## Step 4 - Run local storescp:
>The storescp application implements a Service Class Provider (SCP) for the Storage Service Class. It listens on a specific TCP/IP port for incoming association requests from a SCU and can receive both DICOM images and other DICOM composite objects.

We are going to run a local storescp that will listen for incoming DICOM files. In the next step of this tutorial, we will run another tool (movescu) to tell the Orthanc server to move images to a third-party system - our upcoming local storescp!  

1. In a seperate terminnal session, run the command:  
  `storescp +xa --verbose -od /Users/yonatanmh/Desktop/scp 2000`
     - `+xa` = accept all supported transfer syntaxes
     - `--verbose` = print processing details
     - `-od` = output directory path to write received objects into
     - `2000` = TCP/IP port number to listen to. This should be the same port you configured as the modality's port in Orthanc ConfigOSX.json file.  
2. You should not close the application! Leave it running.
---
## Step 5 - Send DICOM files from Orthanc server to your local storescp via movescu tool:
>The movescu application implements both an SCU for the Query/Retrieve Service Class and an SCP for the Storage Service Class. movescu supports retrieve functionality using the C-MOVE message. It sends query keys to an SCP and awaits responses. It will accept associations for the purpose of receiving images sent as a result of the C-MOVE request.  
The movescu application can initiate the transfer of images to a third party or can retrieve images to itself.  

- In your terminal, run run the command:  
  `movescu localhost 4242 -v -S -aec ORTHANC -aet YONATAN -aem YONATAN --no-port -k QueryRetrieveLevel=Study -k StudyInstanceUID="1.2.826.0.1.3680043.9.6883.1.28291262869281413976408032794530156"`  
  >Replace the AET ("*YONATAN*") with the AET you configured in your Orthanc configOSX file, and change the StudyInstanceUID according to your DICOM files.  
       - `localhost` = Orthanc server hostname
       - `4242` = Orthanct server port
       - `-v` = print processing details    
       - `-S` = use study root information model
       - `-aec` = the called AE title of peer
       - `-aet` = the calling AE title
       - `-aem` = the destination AE title
       - `—no-port` = no port for incoming associations (because the storescp is already configured in Orthanc server)
       - `-k` = matching keys
          - `QueryRetrieveLevel` = what to retrieve
          - `StudyInstanceUID` = the filter to retrieve by

<br>
<br>

---
## Further Reading 
- [DICOM is easy - Introduction to DICOM](http://dicomiseasy.blogspot.com/p/introduction-to-dicom.html)
- [An excellent tutorial for PACS debugging with DCMTK](https://support.dcmtk.org/redmine/projects/dcmtk/wiki/Howto_PACSDebuggingWithDCMTK)
- DCMTK Wiki:
    - https://support.dcmtk.org/docs/storescp.html
    - https://support.dcmtk.org/docs/storescu.html
    - https://support.dcmtk.org/docs/findscu.html
    - https://support.dcmtk.org/docs/movescu.html