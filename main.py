from pydicom import dcmread
from pydicom.uid import ImplicitVRLittleEndian
from pydicom.dataset import Dataset

from pynetdicom import AE
from pynetdicom.sop_class import CTImageStorage, PatientRootQueryRetrieveInformationModelFind, PatientRootQueryRetrieveInformationModelMove, StudyRootQueryRetrieveInformationModelMove 

myAETitle = "YONATAN"
addr = "127.0.0.1"
port = 4242

ae = AE(ae_title=myAETitle)
ae.add_requested_context('1.2.840.10008.1.1') # Verification SOP Class has a UID of 1.2.840.10008.1.1
ae.add_requested_context(CTImageStorage, transfer_syntax=ImplicitVRLittleEndian)
ae.add_requested_context(PatientRootQueryRetrieveInformationModelFind)
ae.add_requested_context(PatientRootQueryRetrieveInformationModelMove)
ae.add_requested_context(StudyRootQueryRetrieveInformationModelMove)

# Associate with a peer AE
assoc = ae.associate(addr, port)

if assoc.is_established:
    print(f"Accespted contexts: {assoc.accepted_contexts}")
    print("------------------")

    # 1 - Send a DIMSE C-ECHO request to the peer
    status = assoc.send_c_echo()
    if status:
        print('C-ECHO Response: 0x{0:04x}'.format(status.Status))
    print("------------------")

    # 2 - Send DIMSE C-STORE request to the peer
    dataset = dcmread('test.dcm')
    status = assoc.send_c_store(dataset)
    if status:
        print('C-STORE Response: 0x{0:04x}'.format(status.Status))
    print("------------------")

    # 3 - Send DIMSE C-FIND request to the peer
    dataset = Dataset()
    dataset.PatientName = '*'
    dataset.QueryRetrieveLevel = "PATIENT"
    responses = assoc.send_c_find(dataset, PatientRootQueryRetrieveInformationModelFind)
    for (status, identifier) in responses:
        if status:
            if status.Status == 65280:
                print('C-FIND Response:')
                print(identifier)
            else:
                print('query status: 0x{0:04X}'.format(status.Status))
        else:
            print('Connection timed out, was aborted or received invalid response')
    print("------------------")

    # 4 - Send DIMSE C-MOVE request to the peer
    dataset = Dataset()
    dataset.StudyInstanceUID="1.2.826.0.1.3680043.9.6883.1.28291262869281413976408032794530156"
    dataset.QueryRetrieveLevel = "STUDY"
    responses = assoc.send_c_move(dataset, move_aet='YONATAN', query_model=StudyRootQueryRetrieveInformationModelMove)
    print('C-MOVE response:')
    for (status, identifier) in responses:
        if status:            
            print('0x{0:04x}'.format(status.Status), end=" ") 
        else:
            print('Connection timed out, was aborted or received invalid response')
    print()
    print("------------------")

    # 5 - Another DIMSE C-MOVE request to the peer
    dataset = Dataset()
    dataset.PatientName = '28291262869281413976408032794530156'
    dataset.StudyInstanceUID="*"
    dataset.QueryRetrieveLevel = "PATIENT"
    responses = assoc.send_c_move(dataset, move_aet='YONATAN', query_model=PatientRootQueryRetrieveInformationModelMove)
    print('C-MOVE response:')
    for (status, identifier) in responses:
        if status:            
            print('query status: 0x{0:04x}'.format(status.Status))
        else:
            print('Connection timed out, was aborted or received invalid response')

    # Release the association
    assoc.release()
